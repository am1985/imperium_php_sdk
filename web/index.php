<?php
/**
 * Set the error reporting and the time zone
 */
date_default_timezone_set('America/Buenos_Aires');
error_reporting(E_ALL);

define('ROOT_PATH', realpath(__DIR__.'/..'));

$loader = require_once __DIR__.'/../vendor/autoload.php';

$app = require_once(ROOT_PATH.'/myCode/RestImperium/app.php');

require_once(ROOT_PATH.'/myCode/RestImperium/controllers.php');

$app->run();

