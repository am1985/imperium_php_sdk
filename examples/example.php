<?php

/**
 * Set the error reporting and the time zone
 */
date_default_timezone_set('America/Buenos_Aires');
error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT_PATH', realpath(__DIR__.'/..'));

$loader = require_once __DIR__.'/../vendor/autoload.php';
$app = require_once(ROOT_PATH.'/myCode/RestImperium/app.php');

use RestImperium\Sdk\Imperium as Imperium;

$id = 100;
$config = array(
    'applicationId'=>$id,
    'applicationKey'=>'key',
    'imperiumRestUrl'=>'http://imperium.rest:9030'
);
$imperiumApiObject = new Imperium($config);
$imperiumApiObject->init();

$isGranted = $imperiumApiObject->isGranted('user', 'ownAccount', 'read');

echo 'In example.php i see:';
var_dump($isGranted);