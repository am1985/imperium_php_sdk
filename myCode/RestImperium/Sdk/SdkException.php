<?php
namespace RestImperium\Sdk;

class SdkException extends Exception
{
    protected $result;

    const CODE_APPLICATION_NOT_FOUND = 1;

    public function __construct($result) 
    {
        $this->result = $result;
        $code = $result['code'];
        $message = $result['errorMessage'];

        parent::__construct($message, $code); 
    }

    public function getResult()
    {
        return $this->result;
    }
}