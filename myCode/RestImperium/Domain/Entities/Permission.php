<?php
namespace RestImperium\Domain\Entities;

use RestImperium\Domain\Entities\Permission as Permission;

class Permission
{
    private $id;
    private $resource;
    private $action;

    public function __construct($id, $resource, $action)
    {
        $this->id = $id;
        $this->resource = $resource;
        $this->action = $action;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function toArray()
    {
        return array(
            'id'=>$this->id,
            'resource'=>$this->resource,
            'action'=>$this->action
        );
    }

    public static function createFromStdClass(\stdClass $data)
    {
        $id = $data->id;
        $resource = $data->resource;
        $action = $data->action;

        return new Permission($id, $resource, $action);
    }

    public function compareByResourceAction(Permission $permission)
    {
        $otherResource = $permission->getResource();
        $otherAction = $permission->getAction();

        $sameResource = (strcasecmp($otherResource, $this->resource)===0);
        $sameAction = (strcasecmp($otherAction, $permission->getAction())===0);

        return ($sameResource&&$sameAction);

    }
}