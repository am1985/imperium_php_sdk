<?php
namespace RestImperium\Domain\Service;

use Silex\Application as ApplicationContainer;

class AbstractService
{
    /*
    *@var ApplicationContainer
    */
    protected $container;

    public function __construct(ApplicationContainer $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }
}
